def main():
    import pyttsx3
    import keyboard
    import time
    import smtplib
    import sys
    import os
    import datetime
    import getpass
    import json
    from pynotifier import Notification
    from email.message import EmailMessage
    os.system('cls')

    with open('config.json', 'r') as c:
        params = json.load(c)["params"]

    def speak(audio):
        engine = pyttsx3.init('sapi5')
        voices = engine.getProperty('voices')
        # print(voices[1].id)
        engine.setProperty('voice', voices[1].id)
        # engine.setProperty('rate', 220)
        engine.say(audio)
        engine.runAndWait()

    def login(receiver, subject, message):
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        # Make sure to give app access in your Google account
        gmaillogin = params['gmaillogin']
        gmailpasslogin = params['gmailpasslogin']

        server.login(gmaillogin, gmailpasslogin)
        email = EmailMessage()
        email['From'] = 'Sender_Email'
        email['To'] = receiver
        email['Subject'] = subject
        email.set_content(message)
        server.send_message(email)

    def password():
        logtime = datetime.datetime.now().strftime('%I:%M %p')
        speak("Tell me the password")

        passinp = getpass.getpass('Type the password to access Friday: ')
        password = "1144"
        password = str(password)
        inp_pass = str(passinp)

        if password == inp_pass:
            try:
                def get_email_info():
                    receiver = "koustav.connect@gmail.com"
                    receiver2 = "linux.hacker@gmail.com"
                    subject = "Access Granted"
                    message = f"Sir, you have successfully logged in to Friday at {logtime}"
                    login(receiver, subject, message)
                    login(receiver2, subject, message)
                print("Password Matched!")
                get_email_info()
                Notification("Access Granted ✔️✔️",
                             "Welcome Back Sir 😊😊", 4).send()
                speak("Access Granted. Welcome Back Sir")

            except Exception as e:
                pass

        else:
            def get_email_info():
                receiver = "koustav.connect@gmail.com"
                subject = "Intruder Alert!"
                message = f"Sir, someone tried to hack me at {logtime}. Please check it immediately"
                login(receiver, subject, message)
            get_email_info()
            speak("Intruder. You are not authorised to access this area")
            Notification("Access not Granted",
                         "You failed to access me 😈😈.", 2).send()
            time.sleep(30)
            sys.exit()

    password()

    try:
        # Importing Modules
        import speech_recognition as sr
        import wikipedia
        import webbrowser
        import random
        import pywhatkit
        import ctypes
        import platform
        import subprocess
        import pyjokes
        import pyautogui
        import requests
        import psutil
        import instaloader
        import winsound
        import cv2
        import speedtest
        import PyPDF2
        import winshell
        import PyDictionary
        from bs4 import BeautifulSoup
        from geopy.geocoders import Nominatim
        import mechanize

        # Global Variables
        wakerep = ["Yes Sir", "Always at your service.", "Okay sir", "Hey sir"]
        wakereprd = random.choice(wakerep)

        chromepath = "C:\\Program Files\\Google\\Chrome Dev\\Application\\chrome.exe"
        strTime = datetime.datetime.now().strftime('%I:%M %p')

        def startup():
            battery = psutil.sensors_battery()
            powerlevel = battery.percent
            hour = int(datetime.datetime.now().hour)
            url = "https://www.google.com/search?q="+"weather"+"Murshidabad"
            html = requests.get(url).content
            soup = BeautifulSoup(html, 'html.parser')
            temp = soup.find(
                'div', attrs={'class': 'BNeawe iBp4i AP7Wnd'}).text
            str = soup.find('div', attrs={'class': 'BNeawe tAd8D AP7Wnd'}).text
            data = str.split('\n')
            timepl = data[0]
            sky = data[1]

            print("Initializing system...")
            speak("initializing system...")

            time.sleep(4)

            if hour >= 0 and hour < 12:
                print("Good Morning!")
                speak("Good Morning!")

            elif hour >= 12 and hour < 18:
                print("Good Afternoon!")
                speak("Good Afternoon!")

            else:
                print("Good Evening!")
                speak("Good Evening!")

            speak("Allow me to introduce myself. I am friday. always ready. I am here to assist you twenty four hours a day and seven days a week")
            print("Allow me to introduce myself. I am friday. always ready. I am here to assist you twenty four hours a day and seven days a week")
            speak("Setting up configurations...")
            print("Setting up configurations...")
            time.sleep(3)
            speak(f"Power level currently at {powerlevel} percent")
            print(f"Power level currently at {powerlevel} %")
            speak(f"Its {timepl} {strTime} and its {temp} and the sky is {sky}")
            print(f"Its {timepl} {strTime} and its {temp} and the sky is {sky}")
            speak("I am ready")

        def wakecommand():
            # It takes microphone input from the user and returns string output

            r = sr.Recognizer()
            with sr.Microphone() as source:
                print("Listening...")
                r.pause_threshold = 1
                audio = r.listen(source)

            try:
                print("Recognizing...")
                query = r.recognize_google(audio, language='en-in')
                print(f"you said: {query}\n")

            except Exception as e:
                # print(e)
                return "None"
            return query

        def takeCommand():
            # It takes microphone input from the user and returns string output

            r = sr.Recognizer()
            with sr.Microphone() as source:
                print("Listening...")
                speak("Listening...")
                r.pause_threshold = 1
                audio = r.listen(source)

            try:
                print("Recognizing...")
                query = r.recognize_google(audio, language='en-in')
                print(f"you said: {query}\n")

            except Exception as e:
                # print(e)
                print("Say that again please...")
                speak("Say that again please")
                return "None"
            return query

        def sendEmail(receiver, subject, message):
            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.starttls()
            # Make sure to give app access in your Google account
            sendmail = params['sendmail']
            sendmailpass = params['sendmailpass']
            server.login(sendmail, sendmailpass)
            email = EmailMessage()
            email['From'] = 'Sender_Email'
            email['To'] = receiver
            email['Subject'] = subject
            email.set_content(message)
            server.send_message(email)

        def feelbore():
            speak("Don't worry sir. I am here. Would you like to play a game?")
            cond = takeCommand().lower()
            while True:
                if "yes" or "yeah" in cond:
                    speak("Tell me which game do you want to play?")
                    speak(
                        "Here is the game list:\n 1)Guessing game \n2)Idragon Adventures \n3)Chicken Catcher\n4)Flappy Bird\n5)Bird Hack")
                    print(
                        "Here is the game list:\n 1)Guessing game \n2)Idragon Adventures \n3)Chicken Catcher\n4)Flappy Bird\n5)Bird Hack")
                    time.sleep(1)
                    game = takeCommand().lower()

                    path1 = "C:\\Users\\techy\\Desktop\\tasks\\Chicken-Catcher"
                    path2 = "C:\\Users\\techy\\Desktop\\tasks\\flappy-bird"
                    path3 = "C:\\Users\\techy\\Desktop\\tasks\\bird-hack"

                    if "guessing game" in game:
                        speak("Be ready for the guessing game!")
                        time.sleep(1)
                        speak("Sir, type your try level?")
                        lev = int(input("Enter try level: "))
                        range = random.randint(1, lev)
                        while True:
                            speak("Please guess the number? ")
                            guess = int(input("Enter your guess level: "))

                            if guess > range:
                                speak("Try smaller")
                                print("Try smaller")
                            elif guess < range:
                                speak("Try greater")
                                print("Try greater")
                            else:
                                speak("Congratulations sir! you guessed it right")
                                print("Congratulations sir! you guessed it right")
                                break

                    elif "idragon adventures" in game:
                        speak("It's a online game. Be ready for it.")
                        time(1)
                        webbrowser.register('chrome',
                                            None,
                                            webbrowser.BackgroundBrowser(chromepath))
                        webbrowser.get('chrome').open(
                            "https://iamkoustav.github.io/idragonAdventures/")

                    elif "chicken catcher" in game:
                        os.chdir(path1)
                        os.system('python main.py')

                    elif "flappy bird" in game:
                        os.chdir(path2)
                        os.system('python main.py')

                    elif "bird hack" in game:
                        os.chdir(path3)
                        os.system('python main.py')

                    elif "choice" in game:
                        lst = []
                        lst.append(path1)
                        lst.append(path2)
                        lst.append(path3)
                        rdchoice = random.choice(lst)
                        os.chdir(rdchoice)
                        os.system('python main.py')

                    else:
                        break

                elif "no" in cond:
                    speak("ok sir")
                    break

        def emergencyhelp(subject, message):
            speak("Asking for help in progress...")
            pywhatkit.send_mail("koustav.connect@gmail.com", "Lkjhg123!",
                                f"{subject}", f"{message}", "techfret.connect@gmail.com")

            pywhatkit.send_mail("koustav.connect@gmail.com", "Lkjhg123!",
                                f"{subject}", f"{message}", "mantu2114@gmail.com")

            pywhatkit.send_mail("koustav.connect@gmail.com", "Lkjhg123!",
                                f"{subject}", f"{message}", "swastikm.cse@gmail.com")

            pywhatkit.send_mail("koustav.connect@gmail.com", "Lkjhg123!",
                                f"{subject}", f"{message}", "mswastikcse@gmail.com")

            pywhatkit.sendwhatmsg_instantly(
                "+919475417623", "Maa ami bipode porechi", wait_time=10, tab_close=False, close_time=3)
            time.sleep(1)
            keyboard.press_and_release('ctrl + w')

            time.sleep(1)

            pywhatkit.sendwhatmsg_instantly(
                "+919434363821", "Baba ami bipode porechi", wait_time=10, tab_close=False, close_time=3)
            time.sleep(1)
            keyboard.press_and_release('ctrl + w')

            time.sleep(1)

            pywhatkit.sendwhatmsg_instantly(
                "+917596890422", "Pudu ami bipode porechi", wait_time=10, tab_close=False, close_time=3)
            time.sleep(1)
            keyboard.press_and_release('ctrl + w')

            speak("Sir! I have asked for help! Don't worry! they will come and help you")

        def taskexecution():
            startup()
            time.sleep(1)

            while True:
                query = takeCommand().lower()

                if 'wikipedia' in query:
                    try:
                        speak('Searching Wikipedia...')
                        query = query.replace("wikipedia", "")
                        results = wikipedia.summary(query, sentences=2)
                        speak("According to Wikipedia")
                        print(results)
                        speak(results)
                    except Exception as e:
                        speak(
                            "Sorry sir could not find results in wikipedia according to you search")

                elif 'time' in query:
                    speak(f"Sir its {strTime}")

                elif 'disconnect to my phone' in query:
                    speak("Ok sir")
                    time.sleep(1)
                    keyboard.press_and_release('windows + b')
                    time.sleep(1)
                    keyboard.press_and_release('enter')
                    time.sleep(1)
                    keyboard.press_and_release('up')
                    time.sleep(1)
                    pyautogui.moveTo(1635, 963)
                    time.sleep(1)
                    pyautogui.rightClick()

                elif 'connect to my phone' in query:
                    print(
                        "Sir, make sure that you AI is opened in your phone and your phone is connected with same wifi")
                    speak(
                        "Sir, make sure that you AI is opened in your phone and your phone is connected with same wifi")
                    time.sleep(2)
                    os.startfile(
                        "E:\\Program Files (x86)\\WOMic\\WOMicClient.exe")
                    time.sleep(1)
                    pyautogui.click(x=585, y=542)
                    time.sleep(1)
                    pyautogui.click(x=598, y=573)
                    time.sleep(1)
                    keyboard.press_and_release('enter')
                    time.sleep(1)
                    keyboard.press_and_release('alt + f4')

                elif 'hack' in query:
                    speak(
                        "Sir, you are taking the risk. Are you sure you want to hack the wifi?")
                    per = takeCommand().lower()
                    if per == "yes":
                        speak("Ok sir.")
                        time.sleep(1)
                        print("Showing you the avialable networks...")
                        speak("Showing you the avialable networks...")
                        time.sleep(1)
                        os.system('netsh wlan show networks')
                        speak("Sir, type the wifi name which i should hack")
                        time.sleep(1)
                        name = input("Enter network: ")
                        print("Wait a minute, hacking the wifi...")
                        speak("Wait a minute, hacking the wifi...")
                        final = f'netsh wlan show profile {name} key=clear'
                        final = final
                        time.sleep(1)
                        os.system(final)
                        speak("Sir the wifi has been hacked successfully")

                    else:
                        speak("Ok sir")

                elif 'lock my screen' in query:
                    speak("Locking screen")
                    time.sleep(1)
                    ctypes.windll.user32.LockWorkStation()
                    speak("Screen locked")

                elif 'unlock' in query:
                    keyboard.press_and_release('spacebar')
                    speak("Enter password to unlock")
                    time.sleep(1)
                    speak("Screen unlocked")

                elif 'upgrade' in query:
                    speak("Ok sir, I am ready for it")
                    speak("Call me again when my developement is completed!")
                    os.system('main.py')
                    time.sleep(1)
                    # pyautogui.click(x=1779, y=776)

                elif 'password' in query:
                    recpass = getpass.getpass(
                        'Type the recent password to change friday password: ')
                    if recpass == "1144":
                        speak("Change the password now!")
                        # os.startfile(r'C:\Users\techy\Desktop\AI\friday')
                        # time.sleep(1)
                        # pyautogui.click(x=1018, y=818)
                        # time.sleep(1)
                        # keyboard.press_and_release('shift + f10')
                        # time.sleep(1)
                        # pyautogui.click(x=331, y=738)
                        os.system('main.py')

                elif 'hide all files' in query:
                    speak("Wait a minute, hiding all files")
                    time.sleep(1)
                    os.system("attrib +h /s /d")
                    speak("Sir, all the files in this folder are now hidden")
                    speak(
                        "Sir, would you like to see the process of making files visible?")
                    cond = takeCommand().lower()
                    if cond == "yes":
                        speak(
                            "Here it is. Open command prompt in the desktop ai friday folder and type this command")
                        os.system('start cmd')

                        print("attrib -h /s /d")
                        speak(
                            "Due to security reasons, I cant tell you the command")

                elif 'show all files' in query:
                    speak("Wait a minute, making visible all files")
                    time.sleep(1)
                    os.system('attrib -h /s /d')
                    speak("All files are visible now!")

                elif 'shut down my computer' in query:
                    speak("shuting down computer")
                    time.sleep(1)
                    os.system("shutdown /s /t 1")
                    speak("Computer shutted down")

                elif 'restart my computer' in query:
                    speak("restarting computer")
                    time.sleep(1)
                    os.system("shutdown /r /t 1")
                    speak("Computer Restarted")

                elif 'sleep my computer' in query:
                    speak("Going on sleep mode on your computer")
                    time.sleep(1)
                    os.system(
                        "rundll32.exe powrprof.dll,SetSuspendState 0,1,0")
                    speak("Computer sleeped")

                elif 'log out my computer' in query:
                    speak("Logging out on your computer")
                    time.sleep(1)
                    os.system("shutdown -l")
                    speak("Computer logged out")

                elif 'recycle bin' in query:
                    winshell.recycle_bin().empty(confirm=False, show_progress=False, sound=True)

                elif 'search in windows' in query:
                    speak("What shoulsd i search for?")
                    ite = takeCommand().lower()
                    keyboard.press_and_release('windows + s')
                    time.sleep(1)
                    keyboard.write(ite)
                    time.sleep(1)
                    keyboard.press('enter')

                elif "ip address" in query:
                    ip = requests.get('https://api.ipify.org').text
                    print(ip)
                    speak(f"Your ip address is {ip}")

                elif "switch the window" in query or "switch window" in query:
                    speak("Okay sir, Switching the window")
                    pyautogui.keyDown("alt")
                    pyautogui.press("tab")
                    time.sleep(1)
                    pyautogui.keyUp("alt")

                elif 'open whatsapp' in query:
                    speak("Opening Whatsapp")
                    time.sleep(1)
                    os.startfile(
                        "C:\\Users\\techy\\AppData\\Local\\WhatsApp\\WhatsApp.exe")
                    time.sleep(1)
                    speak("Whatsapp is opened!")

                elif 'send message' in query:
                    def whatsapmsg(name, message):
                        os.startfile(
                            "C:\\Users\\techy\\AppData\\Local\\WhatsApp\\WhatsApp.exe")
                        time, pyautogui.sleep(10)
                        pyautogui.click(x=255, y=145)
                        time.sleep(1)
                        keyboard.write(name)
                        time.sleep(1)
                        pyautogui.click(x=142, y=284)
                        time.sleep(1)
                        pyautogui.click(x=1327, y=902)
                        time.sleep(1)
                        keyboard.write(message)
                        time.sleep(1)
                        keyboard.press('enter')

                    try:
                        speak("Enter the name correctly")
                        num = input("Enter the name correctly: ")
                        speak("Enter message")
                        mes = input("Enter message: ")
                        whatsapmsg(num, mes)
                        speak("Message delivered")
                    except:
                        speak("Sorry sir unable to dilever message")

                elif 'make a call' in query:
                    def whatsappcall(name):
                        os.startfile(
                            "C:\\Users\\techy\\AppData\\Local\\WhatsApp\\WhatsApp.exe")
                        time, pyautogui.sleep(10)
                        pyautogui.click(x=255, y=145)
                        time.sleep(1)
                        keyboard.write(name)
                        time.sleep(1)
                        pyautogui.click(x=142, y=284)
                        time.sleep(1)
                        pyautogui.click(x=1728, y=73)
                        time.sleep(1)
                    try:
                        speak("Enter the name correctly")
                        num = input("Enter the name correctly: ")
                        whatsappcall(num)
                        speak("Started calling")
                    except:
                        speak("Sorry sir unable to call")

                elif 'make a video call' in query:
                    def whatsappcall(name):
                        os.startfile(
                            "C:\\Users\\techy\\AppData\\Local\\WhatsApp\\WhatsApp.exe")
                        time, pyautogui.sleep(10)
                        pyautogui.click(x=255, y=145)
                        time.sleep(1)
                        keyboard.write(name)
                        time.sleep(1)
                        pyautogui.click(x=142, y=284)
                        time.sleep(1)
                        pyautogui.click(x=1672, y=77)
                        time.sleep(1)
                    try:
                        speak("Enter the name correctly")
                        num = input("Enter the name correctly: ")
                        whatsappcall(num)
                        speak("Started video calling")
                    except:
                        speak("Sorry sir unable to call")

                elif 'play video' in query:
                    speak("What's your topic")
                    topic = takeCommand().lower()
                    time.sleep(1)
                    speak(f"Playing latest vedio related to {topic}")
                    pywhatkit.playonyt(topic)

                elif 'emergency' in query:
                    subject = "Emergency Help! It's Koustav Chakraborty"
                    message = "Ami Sirso! ami bipode porechi!"
                    emergencyhelp(subject, message)

                elif 'search' in query:
                    speak("What should i search for?")
                    search = takeCommand()
                    speak(f"Searching for {search} in google....")
                    time.sleep(1)
                    pywhatkit.search(search)
                    speak(f"Search results for {search}")

                elif 'battery' in query:
                    power = psutil.sensors_battery()
                    percent = power.percent

                    Notification(
                        "Battery Power", f"Battery remaining {100-percent} %", 7).send()
                    speak(f"Battery remaining {100-percent} percent")

                elif 'system' in query:
                    my_system = platform.uname()

                    speak(f"System: {my_system.system}11 Pro")
                    print(f"System: {my_system.system}11 Pro")
                    speak(f"Node Name: {my_system.node}")
                    print(f"Node Name: {my_system.node}")
                    speak(f"Release: {my_system.release}")
                    print(f"Release: {my_system.release}")
                    speak(f"Version: {my_system.version}")
                    print(f"Version: {my_system.version}")
                    speak(f"Machine: {my_system.machine}")
                    print(f"Machine: {my_system.machine}")
                    speak(f"Processor: {my_system.processor}")
                    print(f"Processor: {my_system.processor}")
                    speak(f"Installed ram : 8.00 GB")
                    print(f"Installed ram : 8.00 GB")
                    speak(f"Product Key: Activated")
                    print(f"Product Key : Activated")

                elif 'photo' in query:
                    speak("ok sir!")
                    speak("What should i name the file?")
                    i = takeCommand().lower()
                    camera = cv2.VideoCapture(0)
                    return_value, image = camera.read()
                    cv2.imwrite(i+'.png', image)
                    del(camera)
                    speak("Photo taken")
                    speak("Should i open the file?")
                    cond2 = takeCommand().lower()
                    if cond2 == "yes":
                        op = f"{i}.png"
                        os.startfile(op)
                    else:
                        speak("ok sir!")

                elif 'open camera' in query:
                    speak("opening camera")
                    time.sleep(1)
                    subprocess.run(
                        'start microsoft.windows.camera:', shell=True)
                    speak("Opened camera")
                    time.sleep(1)
                    speak("Sir should i take a photo")
                    ask = takeCommand().lower()
                    if ask == "yes":
                        keyboard.press_and_release('enter')
                        time.sleep(1)
                        speak("Photo taken")
                    elif ask == "no":
                        speak("Ok sir, as you you wish!")
                    else:
                        speak("Sorry sir, unable to understand!")

                elif 'close camera' in query:
                    speak("Closing camera")
                    time.sleep(1)
                    subprocess.run(
                        'Taskkill /IM WindowsCamera.exe /F', shell=True)
                    speak("Closed camera")

                elif 'setting' in query:
                    speak("opening settings")
                    time.sleep(1)
                    # os.startfile("DpiScaling.exe")
                    keyboard.press_and_release('windows + i')
                    speak("Opened settings")

                elif 'file' in query:
                    speak("opening file explorer")
                    time.sleep(1)
                    keyboard.press_and_release('windows + e')
                    speak("Opened file explorer")

                elif 'new desktop' in query:
                    keyboard.press_and_release('ctrl + windows + d')

                elif 'close desktop' in query:
                    keyboard.press_and_release('ctrl + windows + F4')

                elif 'task view' in query:
                    keyboard.press_and_release('windows + tab')
                    time.sleep(1)
                    keyboard.press_and_release('enter')

                elif 'background processes' in query:
                    keyboard.press_and_release('windows + b')
                    keyboard.press_and_release('enter')

                elif 'virus' in query:
                    keyboard.press_and_release('windows + s')
                    time.sleep(1)
                    keyboard.write('security')
                    time.sleep(1)
                    keyboard.press('enter')
                    time.sleep(1)
                    pyautogui.click(x=599, y=332)
                    time.sleep(1)
                    pyautogui.click(x=493, y=420)
                    time.sleep(1)
                    keyboard.press('enter')

                elif 'task manager' in query:
                    keyboard.press_and_release('ctrl + shift + esc')

                elif 'feeling bore' in query:
                    feelbore()

                elif 'volume up' in query:
                    pyautogui.press('volumeup')

                elif 'volume down' in query:
                    pyautogui.press('volumedown')

                elif 'mute volume' in query:
                    pyautogui.press('volumemute')

                elif 'make a folder' in query:
                    speak("Sir, what should i name the folder?")
                    file = takeCommand()
                    speak(f"Wait a minute, making folder as {file}")
                    os.mkdir(file)
                    time.sleep(1)
                    speak("Folder successfully created")
                    os.startfile(file)

                elif 'make a file' in query:
                    speak("Sir, what should i name the file?")
                    file = takeCommand()
                    speak("Please type the extension correctly: ")
                    exten = input("Please type the extension correctly: ")

                    mainfile = f"{file}{exten}"

                    speak(
                        "Type the path correctly. For example 'C:\\Users\\techy\\Desktop\\test2'")
                    # print("Type the path correctly. For example 'C:\\Users\\techy\\Desktop\\test2'")
                    path = input(
                        "Type the path correctly. For example 'C:\\Users\\techy\\Desktop\\test2': ")
                    speak(f"Wait a minute, making file as {mainfile}")

                    with open(os.path.join(path, mainfile), 'w') as fp:
                        pass

                    time.sleep(1)
                    speak(f"{mainfile} successfully created")
                    os.startfile(path)

                elif 'take screenshot' in query:
                    speak("What should i name the file: ")
                    file = takeCommand()
                    img = pyautogui.screenshot()
                    img.save(f"{file}.png")
                    speak("Screenshot taken")
                    speak("Sir, should i open the file")
                    op = takeCommand().lower()
                    if op == "yes":
                        opfi = f"{file}.png"
                        os.startfile(opfi)
                        speak(f"{opfi} opened in photos")
                    else:
                        speak("Ok sir")

                elif 'instagram profile' in query:
                    speak("Enter username correctly of the person: ")
                    dp = input("Enter username correctly of the person: ")
                    webbrowser.register(
                        'chrome', None, webbrowser.BackgroundBrowser(chromepath))
                    webbrowser.get('chrome').open(
                        f"https://www.instagram.com/{dp}")
                    speak(f"Here is the instagram page of {dp}")
                    time.sleep(1)
                    speak(
                        f"Sir if you like the profile picture of {dp}, you can download it")
                    speak(f"Sir, should i download the profile picture?")
                    cond = takeCommand().lower()
                    if cond == "yes":
                        ig = instaloader.Instaloader()
                        speak("Download started..")
                        time.sleep(1)
                        ig.download_profile(dp, profile_pic_only=True)
                        speak("Profile picture successfully download.")
                        speak("Sir, would you like to see the profile picture?.")
                        cond1 = takeCommand().lower()
                        if cond1 == "yes":
                            path = f'{dp}'
                            pic = os.listdir(path)
                            os.startfile(os.path.join(path, pic[0]))

                elif 'joke' in query:
                    for i in range(0, 5):
                        joke = pyjokes.get_joke()
                        print(joke)
                        speak(joke)

                elif 'set alarm' in query:

                    def alarm(Timing, work):
                        altime = str(datetime.datetime.now().strptime(
                            Timing, '%I:%M %p'))
                        altime = altime[11:-3]
                        print(altime)

                        Horeal = altime[:2]
                        Horeal = int(Horeal)
                        Mireal = altime[3:5]
                        Mireal = int(Mireal)
                        speak(f"Done, alarm set for {Timing}")
                        print(f"Done, alarm set for {Timing}")

                        while True:
                            if Horeal == datetime.datetime.now().hour:
                                if Mireal == datetime.datetime.now().minute:
                                    Notification(
                                        f"Alarm of {work}", f"Sir its {Timing}", 7).send()
                                    winsound.PlaySound(
                                        'abc', winsound.SND_LOOP)
                                    speak("Alarm for "+work)
                                elif Mireal < datetime.datetime.now().minute:
                                    break

                    speak(
                        "Sir, please tell me the time to set alarm. For example, set alarm to 5:30 AM")
                    print(
                        "Sir, please tell me the time to set alarm. For example, set alarm to 5:30 AM")
                    tt = takeCommand()
                    tt = tt.replace("set alarm to " or "set alarm ", "")
                    tt = tt.replace(".", "")
                    tt = tt.upper()
                    speak("Sir, please tell me the work?")
                    print("Sir, please tell me the work?")
                    work = takeCommand()
                    alarm(tt, work)

                elif 'temperature' in query:
                    url = "https://www.google.com/search?q="+"weather"+"Raghuathganj"
                    html = requests.get(url).content
                    soup = BeautifulSoup(html, 'html.parser')
                    temp = soup.find(
                        'div', attrs={'class': 'BNeawe iBp4i AP7Wnd'}).text
                    str = soup.find(
                        'div', attrs={'class': 'BNeawe tAd8D AP7Wnd'}).text
                    data = str.split('\n')
                    timepl = data[0]
                    sky = data[1]
                    speak(
                        f"Its {timepl} {strTime} and its {temp} and the sky is {sky}")
                    print(
                        f"Its {timepl} {strTime} and its {temp} and the sky is {sky}")

                elif 'location' in query:
                    r = requests.get('https://get.geojs.io/')
                    ip_request = requests.get(
                        'https://get.geojs.io/v1/ip.json')
                    ipAdd = ip_request.json()['ip']

                    url = 'https://get.geojs.io/v1/ip/geo/'+ipAdd+'.json'
                    geo_request = requests.get(url)
                    geo_data = geo_request.json()

                    # Initialize Nominatim API
                    geolocator = Nominatim(user_agent="geoapiExercises")
                    Latitude = geo_data['latitude']
                    Longitude = geo_data['longitude']
                    print("Latitude: ", Latitude)
                    print("Longitude: ", Longitude)
                    location = geolocator.geocode(Latitude+","+Longitude)
                    print(location)
                    speak(f"Sir I think we are in {location}")

                elif 'number' in query:
                    mc = mechanize.Browser()
                    mc.set_handle_robots(False)

                    url = 'https://www.findandtrace.com/trace-mobile-number-location'
                    mc.open(url)

                    mc.select_form(name='trace')
                    speak("Enter the mobile number with country code immediatly")
                    mc['mobilenumber'] = input(
                        "Enter the mobile number(with country code): ")  # Enter a mobile number
                    res = mc.submit().read()

                    soup = BeautifulSoup(res, 'html.parser')
                    tbl = soup.find_all('table', class_='shop_table')
                    # print(tbl)

                    data = tbl[0].find('tfoot')
                    c = 0
                    for i in data:
                        c += 1
                        if c in (1, 4, 6, 8):
                            continue
                        th = i.find('th')
                        td = i.find('td')
                        print(th.text, td.text)

                    data = tbl[1].find('tfoot')
                    c = 0
                    for i in data:
                        c += 1
                        if c in (2, 20, 22, 26):
                            th = i.find('th')
                            td = i.find('td')
                            print(th.text, td.text)

                elif 'take note' in query:
                    speak("Sir, what should i write?")
                    note = takeCommand()
                    os.system('start notepad')
                    time.sleep(1)
                    keyboard.write(note)
                    keyboard.press('enter')
                    speak("Sir should i save it?")
                    per = takeCommand().lower()
                    if per == "yes":
                        keyboard.press_and_release('ctrl + s')
                        speak("What should i name the file?")
                        name = takeCommand()
                        keyboard.write(name)
                        time.sleep(1)
                        keyboard.press_and_release('enter')
                        time.sleep(1)
                        speak(f"File saved as {name}")

                elif 'show desktop' in query:
                    keyboard.press_and_release('windows + d')

                elif 'internet speed' in query:
                    speak("Fetching datas...")
                    print("Fetching datas...")

                    st = speedtest.Speedtest()
                    download = st.download()
                    download = int(int(download/80000))
                    upload = st.upload()
                    upload = int(int(upload/80000))

                    print(
                        f"Sir, the download speed is {download} MB/sec and the upload speed is {upload} MB/sec")
                    speak(
                        f"Sir, the download speed is {download} MB per second and the upload speed is {upload} MB per second")

                elif 'desktop background' in query:
                    speak("Type the new background picture path carefully")
                    path = input("Type the new background picture path: ")
                    ctypes.windll.user32.SystemParametersInfoW(
                        20, 0, path, 0)
                    speak("Background changed successfully")
                    keyboard.press_and_release('windows + d')
                    time.sleep(6)
                    keyboard.press_and_release('windows + d')

                elif 'news' in query:
                    def NewsFromBBC():
                        query_params = {
                            "source": "the-times-of-india",
                            "sortBy": "top",
                            "apiKey": "4ae31099860c4a47b3d1d5a760aa26aa"
                        }
                        main_url = " https://newsapi.org/v1/articles"

                        # fetching data in json format
                        res = requests.get(main_url, params=query_params)
                        open_bbc_page = res.json()

                        # getting all articles in a string article
                        article = open_bbc_page["articles"]

                        # empty list which will
                        # contain all trending news
                        results = []

                        for ar in article:
                            results.append(ar["title"])

                        for i in range(len(results)):

                            # printing all trending news
                            print(i + 1, results[i])
                            speak(i + 1, results[i])
                    try:
                        speak(
                            "Getting today's headlines from the times of india")
                        NewsFromBBC()
                    except:
                        speak("Sorry sir, unable to fetch news")

                elif 'coding' in query:
                    speak("Type the filename with extension correctly")
                    mainfile = input(
                        "Type the filename with extension correctly: ")

                    speak(
                        "Type the path correctly. For example 'C:\\Users\\techy\\Desktop\\test2' or to make the file in present folder type 'this folder' ")
                    # print("Type the path correctly. For example 'C:\\Users\\techy\\Desktop\\test2'")
                    path = input(
                        "Type the path correctly. For example 'C:\\Users\\techy\\Desktop\\test2' or to make the file in present folder type 'this folder' : ")
                    speak(f"Wait a minute, making file as {mainfile}")

                    with open(os.path.join(path, mainfile), 'w') as fp:
                        pass

                    os.startfile(mainfile)
                    time.sleep(1)
                    keyboard.write(
                        "Coding Environment setup successfull!")
                    time.sleep(0.2)
                    keyboard.press_and_release('ctrl + /')
                    time.sleep(0.2)
                    keyboard.press_and_release('enter')
                    time.sleep(0.2)
                    keyboard.write("Start Coding...")
                    time.sleep(0.2)
                    keyboard.press_and_release('ctrl + /')
                    time.sleep(0.2)
                    speak(f"Sir, now you can start coding in {mainfile}")

                elif 'dictionary' in query:

                    class Dict:
                        def Dictionary(self):
                            dic = PyDictionary.PyDictionary()
                            speak(
                                "Which word do u want to find the meaning sir")

                            # Taking the string input
                            main = takeCommand().lower()
                            word = dic.meaning(main)
                            print(len(word))

                            for state in word:
                                meaning = word[state]
                                speak("the meaning is" + meaning)

                    if __name__ == '__main__':
                        Dict()
                        Dict.Dictionary(self=None)

                elif 'open youtube' in query:
                    speak("What should i search in youtube")
                    url = takeCommand()
                    speak(f"Searching for {url} in youtube")
                    time.sleep(1)
                    webbrowser.register('chrome',
                                        None,
                                        webbrowser.BackgroundBrowser(chromepath))
                    webbrowser.get('chrome').open(
                        f"https://www.youtube.com/results?search_query={url}")
                    speak(f"Search results for {url} in youtube")

                elif 'play the video' in query:
                    keyboard.press_and_release('windows + 1')
                    time.sleep(1)
                    keyboard.press_and_release('k')

                elif 'open site' in query:
                    speak("Which site should i open?")
                    url = takeCommand().lower()
                    speak(f"Launching {url}....")
                    time.sleep(1)
                    webbrowser.register('chrome',
                                        None,
                                        webbrowser.BackgroundBrowser(chromepath))
                    webbrowser.get('chrome').open(f"https://www.{url}")

                elif 'open geeks' in query:
                    gfg = params['gfg_url']
                    webbrowser.register('chrome',
                                        None,
                                        webbrowser.BackgroundBrowser(chromepath))
                    webbrowser.get('chrome').open(gfg)

                elif 'channel' in query:
                    youtube_url = params['youtube_url']
                    webbrowser.register('chrome',
                                        None,
                                        webbrowser.BackgroundBrowser(chromepath))
                    webbrowser.get('chrome').open(youtube_url)

                elif 'blog' in query:
                    medium_url = params['medium_url']
                    webbrowser.register('chrome',
                                        None,
                                        webbrowser.BackgroundBrowser(chromepath))
                    webbrowser.get('chrome').open(medium_url)

                elif 'recent mail' in query:
                    webbrowser.register('chrome',
                                        None,
                                        webbrowser.BackgroundBrowser(chromepath))
                    webbrowser.get('chrome').open(
                        f"https://mail.google.com/mail/u/0/?tab=rm&ogbl#inbox")
                    time.sleep(1)
                    pyautogui.click(x=968, y=314)
                    speak("Sir, this is your recent mail")

                elif 'pdf' in query:
                    book = open('pdf1.pdf', 'rb')
                    pdfReader = PyPDF2.PdfFileReader(book)
                    pages = pdfReader.numPages
                    speak(f"Total number of pages are {pages}")
                    speak("Enter the page number which i have to read")
                    pg = int(
                        input("Enter the page number which I have to read: "))
                    page = pdfReader.getPage(pg)
                    text = page.extractText()
                    speak(text)

                elif 'play music' in query:
                    music_dir = 'C:\\Users\\techy\\Music'
                    songs = os.listdir(music_dir)
                    rd = random.choice(songs)
                    print(f"Songs Avialable: {songs}")
                    os.startfile(os.path.join(music_dir, rd))

                elif 'open chrome' in query:
                    speak("Opening Chrome")
                    time.sleep(1)
                    os.startfile(chromepath)
                    speak("Chrome is opened")

                elif 'open new tab' in query:
                    keyboard.press_and_release('windows + 1')
                    time.sleep(1)
                    keyboard.press_and_release('ctrl + t')

                elif 'close tab' in query:
                    keyboard.press_and_release('windows + 1')
                    time.sleep(1)
                    keyboard.press_and_release('ctrl + w')

                elif 'open incognito' in query:
                    keyboard.press_and_release('windows + 1')
                    time.sleep(1)
                    keyboard.press_and_release('ctrl + shift + n')

                elif 'close chrome' in query:
                    speak("closing Chrome")
                    time.sleep(1)
                    os.system("TASKKILL /F /IM chrome.exe")
                    speak("Chrome is closed")

                elif 'open code' in query:
                    codePath = "E:\\Users\\techy\\AppData\\Local\\Programs\\Microsoft VS Code\\Code.exe"
                    speak("Opening visual studio code")
                    time.sleep(1)
                    os.startfile(codePath)
                    speak("Opened visual studio code")

                elif 'close code' in query:
                    speak("closing visual studio code")
                    time.sleep(1)
                    os.system("TASKKILL /F /IM Code.exe")
                    speak("Visual studio code  is closed")

                elif 'open editor' in query:
                    edipath = "E:\\Program Files (x86)\\VidClippe\\XJEditor.exe"
                    speak("Opening vidclipper")
                    time.sleep(1)
                    os.startfile(edipath)
                    speak("Opening vidclipper")

                elif 'close editor' in query:
                    speak("closing vidclipper")
                    time.sleep(1)
                    os.system("TASKKILL /F /IM XJEditor.exe")
                    speak("vidclipper  is closed")

                elif 'open notepad' in query:
                    speak("Opening notepad")
                    time.sleep(1)
                    os.system('start notepad')
                    speak("Opening notepad")

                elif 'close notepad' in query:
                    speak("closing notepad")
                    time.sleep(1)
                    os.system("TASKKILL /F /IM notepad.exe")
                    speak("notepad  is closed")

                elif 'open github desktop' in query:
                    gitdesk = "C:\\Users\\techy\\AppData\\Local\\GitHubDesktop\\GitHubDesktop.exe"
                    speak("Opening github desktop")
                    time.sleep(1)
                    os.startfile(gitdesk)
                    speak("Opened github desktop")

                elif 'close github desktop' in query:
                    speak("closing github desktop")
                    time.sleep(1)
                    os.system("TASKKILL /F /IM GitHubDesktop.exe")
                    speak("github desktop  is closed")

                elif 'open zoom' in query:
                    zoom = "C:\\Users\\techy\\AppData\\Roaming\\Zoom\\bin\\Zoom.exe"
                    speak("Opening zoom")
                    time.sleep(1)
                    os.startfile(zoom)
                    speak("Opened zoom")

                elif 'close zoom' in query:
                    speak("closing zoom")
                    time.sleep(1)
                    os.system("TASKKILL /F /IM Zoom.exe")
                    speak("zoom  is closed")

                elif 'email' in query:
                    try:
                        def get_email_info():
                            speak('To Whom you want to send email')
                            # name = get_info()
                            speak("Enter the gmail id to send email: ")
                            receiver = input(
                                "Enter the gmail id to send email: ")
                            print(receiver)
                            speak('What is the subject of your email?')
                            subject = takeCommand()
                            speak('Tell me the message in your email')
                            message = takeCommand()
                            speak(
                                "Sir, are you sure that you want to send the mail? Let me show you the details:")
                            print(f"To: {receiver}")
                            speak(f"To: {receiver}")
                            print(f"Subject: {subject}")
                            speak(f"Subject: {subject}")
                            print(f"Message: {message}")
                            speak(f"Message: {message}")

                            conf = takeCommand().lower()
                            print(conf)
                            if conf == "yes":
                                sendEmail(receiver, subject, message)
                                speak(
                                    f'Your email has been sent to {receiver}')
                                print(
                                    f'Your email has been sent to {receiver}')
                                speak("Let me show you the mail sir")
                                print("Let me show you the mail sir")
                                time.sleep(1)
                                webbrowser.register('chrome',
                                                    None, webbrowser.BackgroundBrowser(chromepath))
                                webbrowser.get('chrome').open(
                                    f"https://mail.google.com/mail/u/0/?tab=rm&ogbl#sent")

                                speak('Do you want to send more email?')
                                send_more = takeCommand()

                                if 'yes' in send_more:
                                    get_email_info()

                                else:
                                    speak("Ok sir")
                            else:
                                speak("Ok sir")

                        get_email_info()
                    except Exception as e:
                        print(e)
                        speak("Sorry sir. I am not able to send this email")

                elif 'sleep' in query:
                    speak(
                        "Ok sir i am going to sleep. just say wake up and i will be online again")
                    print(
                        "Ok sir I am going to sleep. just say 'wake up friday' and I will be online again")
                    break

        if __name__ == "__main__":

            while True:
                permission = wakecommand().lower()
                if "wake up" in permission:
                    os.system('cls')
                    reply = wakereprd
                    speak(reply)
                    print(reply)
                    Notification("Friday", "Friday is Activated", 7).send()
                    taskexecution()

                elif "bye" in permission:
                    speak("Thanks for using me sir and  have a good day")
                    # Friday offline status
                    print("Friday is offline now")
                    pyttsx3.speak("Friday is offline now")
                    time.sleep(1)
                    pyttsx3.speak("Type 'yes' for getting friday online")
                    print("Type 'yes' for getting friday online")
                    start = input("Should I online the friday?")

                    if "yes" in start:
                        os.chdir(r"C:\Users\techy\Desktop\AI\friday")
                        pyttsx3.speak("Getting Friday online....")
                        time.sleep(1)
                        pyttsx3.speak("Friday is online now")
                        print("Friday is online now")
                        os.system('python main.py')

                    else:
                        pyttsx3.speak("ok")
                        print("ok")
                        os.system('cls')
                        break

    except Exception as e:
        print(f"\nError : {e}\n")
        Notification("Internet Connection Error",
                     "Sir, you need an active internet connection", 7).send()
        speak("Unable to connect internet")
        time.sleep(1)
        speak(
            "Sir you need an active internet connection. should i turn on the wifi?")
        per = input("Should I turn on the wifi?: ")
        if per == "yes":
            speak("Turning on wifi...")
            keyboard.press_and_release('windows + a')
            time.sleep(1)
            keyboard.press_and_release('enter')
            time.sleep(1)
            keyboard.press_and_release('windows + a')
            Notification("Internet Connection Successful",
                         "Sir, you are now connected to internet", 7).send()
            speak(
                "Internet Connection Successful. Sir, you are now connected to internet")
            os.system('python main.py')

        else:
            print(
                "Ok sir. But I need a active internet connection to work with you. Anyway sir, I am leaving...")
            speak(
                "Ok sir. But I need a active internet connection to work with you. Anyway sir, I am leaving...")
            sys.exit()


main()
